function [muf,Pf,wf,mufu, Pfu] = GPB2_FILTER(JMLS,mu,P,w, u, ym, plant_params)
% Log weight implementation of a GPB2 Filter

% Get dimensions
nx = plant_params.Dimms.nx;
nz = JMLS.N;

% Allocate space
muf = nan(nx,nz);
Pf = nan(nx,nx,nz);
wf = nan(nz,1);

for zf = 1:nz % next discrete state
    
    % Unpack model
    A = JMLS.model(zf).A;
    B = JMLS.model(zf).B;
    Q = JMLS.model(zf).Q;
    C = JMLS.model(zf).C;
    D = JMLS.model(zf).D;
    R = JMLS.model(zf).R;

    % KF prediction
    mup = A*mu + B*u;
    Pp = nan(nx,nx,nz);
    for i=1:nz
        Pp(:,:,i) = A*P(:,:,i)*A' + Q;
    end
    
    % KF correction
    Pfm = nan(nx,nx,nz);
    mufm = nan(nx,nz);
    logML = nan(nz,1);
    muyerr = ym - (C*mup + D*u); % Measurement Prediction error
    for i = 1:nz
        Py = C*Pp(:,:,i)*C' + R;          
        Kg = Pp(:,:,i)*C'/Py;
        es = muyerr(:,i);
        mufm(:,i) = mup(:,i) + Kg*es; 
        Pfm(:,:,i) = Pp(:,:,i) - Kg*Py*Kg';
        logML(i) = -0.5*log(det(2*pi*Py)) -0.5*es'/Py*es;
    end
    
    % Calculate weights
    wfm = w + JMLS.T(zf,:)' + logML;

    % Collapse the predicted down to one unimodal gaussian
    wzf = LSE(wfm);
    wfm = normalise_log_weights(wfm);
    [~, ~, muzf, Pzf] = KL_GMM_reduction_logw(1,1,1, nz, wfm, mufm, Pfm);
    
    % Same mode for the new discrete state
    muf(:,zf) = muzf;
    Pf(:,:,zf) = Pzf;
    wf(zf) = wzf;

end


wf = normalise_log_weights(wf);
% Reduce to one mode
[~, ~, mufu, Pfu] = KL_GMM_reduction_logw(1,1,1, nz, wf, muf, Pf);
