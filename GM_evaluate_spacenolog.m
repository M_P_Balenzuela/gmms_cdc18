function GMeval = GM_evaluate_space(ws,mus,Ps,space)

Sd = length(space);

GMeval = zeros(1,Sd);
for i =1:length(ws)
    P = Ps(:,:,i);
    Pinvsq = chol(P^-1);
    es = mus(:,i) - space;
    es = Pinvsq * es;
    es = es .* es;
    ML = sum(es,1);
    ML = -0.5*ML;
    ML = exp(ML);
    nc = (det(2*pi*P))^-0.5;
    ML = nc * ML;
    ML = ws(i)*ML;
    GMeval = GMeval + ML;
end