function wT = plot_PS(ym,u,uf,space,MM,PM,xf,wf,xsf,wsf,plant_params)

% M = size(xf,2);
M = length(space);
Mf = size(xsf,2);
spaceres = space(2)-space(1);

%  [~,Sd] = size(space);%

    %     mup = nan(nx,M);
        for l = 1:PM.N
            wm = PM.model(l).wm;
            A=PM.model(l).A;
            B=PM.model(l).B;
            Q=PM.model(l).Q;
            for ks = 1:length(wf)
                % Get SS for PM mode

                
                mupi = A*xf(:,ks)+B*u;
                

                Qinvsq = chol(Q^-1);

                es = space - mupi;
                es = Qinvsq * es;
                es = es .* es;

                ML = log(det(2*pi*Q)) + sum(es,1);
                ML = -0.5*ML;
                if ((ks==1) && (l==1))
                    PSr = wf(ks) + log(wm) + ML;
                else
                    PSr2 = [PSr; wf(ks)+log(wm)+ML];
                    PSr = LSE(PSr2);%PSr + wf(ks)*wm*exp(ML);
                end
                
    %                     mup(:,ks) =  mupi;          
            end

        end


        PSr = normalise_log_weights(PSr);

            


    %     measLike = nan(MM.N,M);

        for l = 1:MM.N

            wm = MM.model(l).wm;
            % Predict measurement using MM
            C = MM.model(l).C;
            D = MM.model(l).D;
            R = MM.model(l).R;
            

            muy = C*space+D*u;
      

            Rinvsq = chol(R^-1);

            es = ym - muy;
            es = Rinvsq * es;
            es = es .* es;

            ML = log(det(2*pi*R)) + sum(es,1);
            ML = -0.5*ML;
            if (l==1)
                PSrm = log(wm) + ML;
            else
                PSrm2 = [PSrm; log(wm)+ML];
                PSrm = LSE(PSrm2);  %MM down, particles across
            end
        end


        PSr = PSr + PSrm;
        
        PSr = normalise_log_weights(PSr);

        wT = PS_matlab_diffM_wrap(M,PSr',space,Mf,wsf,xsf,uf,plant_params,PM);
        
        
%         wT = PS_logw_reweight(Sd,PSr',space,length(wsf),wsf,xsf,uf,plant_params,PM);
        wT = normalise_log_weights(wT);
        wT = wT - log(spaceres);
        wT = exp(wT);
        wT = wT';