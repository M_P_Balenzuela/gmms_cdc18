function [mus,Ps,ws, musu, Psu] = GPB2_SMOOTHER(JMLS,muskp1,Pskp1,wskp1,muf,Pf,wf,u,plant_params)
%Article (kim1994dynamic) Kim, C.-J. Dynamic linear models with Markov-switching Journal of Econometrics, Elsevier, 1994, 60, 1-22

% unpack Dimensions
nx = plant_params.Dimms.nx;
nz = JMLS.N;

% Allocate space
musi = nan(nx,nz,nz);
Psi = nan(nx,nx,nz,nz);
wsi = nan(nz,nz);
for zs = 1:nz
    for z = 1:nz
        mufz = muf(:,z);
        Pfz = Pf(:,:,z);
    
        % Unpack model
        A = JMLS.model(zs).A;
        B = JMLS.model(zs).B;
        Q = JMLS.model(zs).Q;
        
        % Perform RTS prediction
        mup = A*mufz + B*u;
        Pp = A*Pfz*A' + Q;
        
        % Perform RTS correction
        G = Pfz*A'/Pp;
        musi(:,zs,z) = mufz + G*(muskp1(:,zs) - mup);
        Psi(:,:,zs,z) = Pfz + G*(Pskp1(:,:,zs) - Pp)*G';        
    end
    % Weights
    wsim =  JMLS.T(zs,:)' + wf;
    wsi(zs,:) = normalise_log_weights(wsim)' + wskp1(zs);

end

% Allocate space
mus = nan(nx,nz);
Ps = nan(nx,nx,nz);
ws = nan(nz,1);
for z = 1:nz
    % form unimodal gaussian for each filtered mode in time k
    wn = normalise_log_weights(wsi(:,z));
    [~, ~, musu, Psu] = KL_GMM_reduction_logw(1,1,1, nz, wn, musi(:,:,z), Psi(:,:,:,z));
    mus(:,z) = musu;
    Ps(:,:,z) = Psu;    
    ws(z) = LSE(wsi(:,z));
end

ws = normalise_log_weights(ws);

% Reduce to one mode
[~, ~, musu, Psu] = KL_GMM_reduction_logw(1,1,1, nz, ws, mus, Ps);