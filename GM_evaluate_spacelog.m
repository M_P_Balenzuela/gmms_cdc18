function GMeval = GM_evaluate_spacelog(ws,mus,Ps,space)

for i =1:length(ws)
    P = Ps(:,:,i);
    Pinvsq = chol(P^-1);
    es = mus(:,i) - space;
    es = Pinvsq * es;
    es = es .* es;
    ML = sum(es,1);
    nc = log(det(2*pi*P));
    ML = ML + nc;
    ML = -0.5*ML;
    ML = ML + ws(i);
    
    
    if i==1
       GMeval = ML; 
    else
        GMeval2 = [GMeval; ML];
        GMeval = LSE(GMeval2);
    end
end

GMeval = exp(GMeval);