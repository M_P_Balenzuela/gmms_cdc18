function wout = LSE(win)
wstar = max(win);
wout = wstar + log(sum(exp(win-wstar)));