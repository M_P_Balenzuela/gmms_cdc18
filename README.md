**This code accompanies the paper:**
*Balenzuela, M.P., Dahlin, J., Bartlett, N., Wills, A.G., Renton, C. and Ninness, B., 2018, December. Accurate Gaussian Mixture Model Smoothing using a Two-Filter Approach. In 2018 IEEE Conference on Decision and Control (CDC) (pp. 694-699). IEEE.*

The code can be run by calling *main.m* or *main_batch.m* from the MATLAB console.

Copyright M. P. Balenzuela 2018