function plot_5(x,y,i)

switch i
    case 1
        s = '-';
        RGB =['7f'; 'c9'; '7f'];%green
        w = 8;
        RGBi = hex2dec(RGB)/255;
        plot(x,y,s,'Color',RGBi,'LineWidth',w);
%         line_fewer_markers(x,y,20,s,'Color',RGBi,'LineWidth',w, 'Spacing', 'curve');
        
    case 4
        s = '-*';         
        RGB =['fd'; 'c0'; '86'];%orange        
        w = 2;
        RGBi = hex2dec(RGB)/255;
        line_fewer_markers(x,y,20,s,'Color',RGBi,'LineWidth',w, 'Spacing', 'curve');

    case 2
        s = '-o';
        RGB =['38'; '6c'; 'b0'];%blue
        w = 2;
        RGBi = hex2dec(RGB)/255;
        line_fewer_markers(x,y,20,s,'Color',RGBi,'LineWidth',w, 'Spacing', 'curve');
        
    case 3
        s = '-s';
%         RGB =['7f'; 'c9'; '7f'];%green
%         RGB =['fd'; 'c0'; '86'];%orange
        RGB =['be'; 'ae'; 'd4'];%light purple
        w = 2;
        RGBi = hex2dec(RGB)/255;
%         plot(x,y,s,'Color',RGBi,'LineWidth',w);
        line_fewer_markers(x,y,20,s,'Color',RGBi,'LineWidth',w, 'Spacing', 'curve');
    case 5
        s = ':';
        RGB =['f0'; '02'; '7f'];% magenta
        RGBi = hex2dec(RGB)/255;
        w=3;
        plot(x,y,s,'Color',RGBi,'LineWidth',w);
        
        
end


if i == 1
    set(gca,'fontsize', 18);
end
hold on;
