function [cTVD,cKL,csKL] = evaluate_divergences(GMMS,aSLDS,GPB2s,IMMS,PF,PS,PF_PRIOR,uidx,yidx,MM,PM,plant_params)

[~,N] = size(uidx);

xmax = nan(4,1);
xmin = nan(4,1);

TVD = nan(4,1);
KL = nan(4,1);
sKL = nan(4,1);

    
cTVD = 0;
cKL = 0;
csKL = 0;
for k = 1:N-1
    k
    tic
    % deturmine space containing most of the probability mass
    plotSD = 5; % Let's have 5 SD's of Gaussian mixtures 
    [xmin(1),xmax(1)] = GM_plotrange(GMMS.den(k).mu,  GMMS.den(k).P,  plotSD);
    [xmin(2),xmax(2)] = GM_plotrange(aSLDS.den(k).mu,  aSLDS.den(k).P,  plotSD);
    [xmin(3),xmax(3)] = GM_plotrange(GPB2s.den(k).mu,  GPB2s.den(k).P,  plotSD);
    [xmin(4),xmax(4)] = GM_plotrange(IMMS.den(k).mu,  IMMS.den(k).P,  plotSD);
    
    sL = min(xmin);
    sM = max(xmax);
    
    space =  sL:3*10^-3:sM;
    
    mus = GMMS.den(k).mu;
    Ps = GMMS.den(k).P;
    ws = GMMS.den(k).w;
    GMMSe = GM_evaluate_spacelog(ws,mus,Ps,space);
    

    mus = aSLDS.den(k).mu;
    Ps = aSLDS.den(k).P;
    ws = aSLDS.den(k).w;
    aSLDSe = GM_evaluate_spacelog(ws,mus,Ps,space);
    
    mus = GPB2s.den(k).mu;
    Ps = GPB2s.den(k).P;
    ws = GPB2s.den(k).w;
    GPB2e = GM_evaluate_spacelog(ws,mus,Ps,space);
    
    mus = IMMS.den(k).mu;
    Ps = IMMS.den(k).P;
    ws = IMMS.den(k).w;    
    IMMe = GM_evaluate_spacelog(ws,mus,Ps,space);
    
    ym = yidx(:,k);
    u = uidx(:,k);
    uf = uidx(:,k+1);
    % Now plot PS result
    if (k==1)
       xf = PF_PRIOR;
       [~,M] = size(xf);
       wf = -log(M)*ones(M,1);
    else
        wf = PF.den(k-1).w;
        xf = PF.den(k-1).mu;
    end

    wsf = PS.den(k+1).w;
    xsf = PS.den(k+1).mu;

    PSe = plot_PS(ym,u,uf,space,MM,PM,xf,wf,xsf,wsf,plant_params);
    
    
    [TVD(1),KL(1),sKL(1)] = density_divergance(PSe, GMMSe); % P first
    [TVD(2),KL(2),sKL(2)] = density_divergance(PSe, aSLDSe);
    [TVD(3),KL(3),sKL(3)] = density_divergance(PSe, GPB2e);
    [TVD(4),KL(4),sKL(4)] = density_divergance(PSe, IMMe);
    
    cTVD = cTVD + TVD;
    cKL = cKL + KL;
    csKL = csKL + sKL;
    cKL
    toc
    
%     figure;
%     plot(GMMSe)
%     hold on
%     plot(aSLDSe)
%     plot(GPB2e)
%     plot(IMMe)
%     plot(PSe)
    
end

