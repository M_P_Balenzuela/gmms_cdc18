function [k, w, u, P] = KL_GMM_reduction_logw(Ml, Mu, lambda, ~, logw, u, P)
% Just a wrapper function

[k, w, u, P] = GM_reduction_KL(logw, u, P, Mu, Ml , lambda);

