while(1)
%% Clear workspace
clear all;
% close all;
clc;


M_PF_NUMBER = 10^5;%10^5;%*10^3; % must be even for the prior chosen



rng('shuffle');
rngseed = round(rand*10^6)
rng(rngseed);

main_common;

[cTVD,cKL,csKL] = evaluate_divergences(GMMS,aSLDS,GPB2s,IMMS,PF,PS,PF_PRIOR,uidx,yidx,MM,PM,plant_params)

 disp('FINISHED MAKING one');
 
 save(['rng_seed_' num2str(rngseed)]);
end
