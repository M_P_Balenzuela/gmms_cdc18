function [mu,P] = normal_combine_two_modes(mu1,P1,mu2,P2)

P1inv = P1^(-1);
P2inv = P2^(-1);

P = (P1inv + P2inv)^(-1);
mu = P*(P1inv*mu1 + P2inv*mu2);