#define varint int

#include "mex.h"
#include "math.h"
#include <stdlib.h>


#define LOG2PI 1.8378770664093453390819377091248 //ln(2.0*pi), used "vpa(log(2.0*pi))" in matlab R2018a





double LSE_pair(double a, double b)
{
    if (a<b)
    {
        // swap a and b
        double c = a;
        a = b;
        b = c;
    }
        // assuming a is larger
        b = b-a;
        b = exp(b);
        b = b + 1.0;
        b = log(b);
        a = a + b;
        return(a);        
        
}

// LSE over the second dimension, write over first column with result
void LSE_matrix(double *A, int M, int N)
{
    
    /*
    int i, j;
    for (i=0;i<M;++i)
    {
        A[i] = LSE_pair(A[i], A[i+M]);
    }
            
            
    */
    int i, j;
    for (i=0;i<M;++i)
    {
        // Find max value
        double max_val, val;
        max_val = A[i];// + 0*j
        for (j=1;j<N;++j)
        {
            val = A[i+M*j];
            if (val > max_val)
            {
                max_val = val;
            }
        }
        // Get sum(exp(x-x*))
        double cumsum = 0.0;
        for (j=0;j<N;++j)
        {
            cumsum += exp(A[i+M*j] - max_val);
        }
        A[i] = log(cumsum) + max_val;
        
    }
}

void normalise_log_weights(double *lnw, int M)
{
    // Find max value
    int i;
    double cumsum, maxlnw;
    maxlnw = lnw[0];
    for(i=1;i<M;++i)
    {
        if (maxlnw < lnw[i])
        {
            maxlnw = lnw[i];
        }
    }

    // sum(exp(lnw-maxlnw))
    cumsum = 0.0;
    for(i=0;i<M;++i)
    {
        cumsum = cumsum + exp(lnw[i]-maxlnw);
    }
    cumsum = log(cumsum);
    cumsum = cumsum + maxlnw;
    cumsum = -cumsum;
    
    
    for(i=0;i<M;++i)
    {
        lnw[i] = lnw[i] + cumsum;
    }

}

void linearPrediction(int nx, int nu, int M, double *xp, double *A, double *x, double *B, double *u)
{ //xp = Ax + B*u, where x is nx by M, and u is nu by 1
int i,j,m;
for(i=0;i<nx;++i)
{
	xp[i] = 0.0;
	// calc. Bu comp.
	for(j=0;j<nu;++j)
	{
		xp[i] = xp[i] + B[i+nx*j]*u[j];
	}

    
	// Bu is common for every particle
	for(m=1;m<M;++m)
	{
		xp[i+nx*m] = xp[i];
	}
	
	// add Ax comp.
    
	for(m=0;m<M;++m)
	{
		for(j=0;j<nx;++j)
		{
			xp[i+nx*m] = xp[i+nx*m] + A[i+nx*j]*x[j+nx*m];
		}
	}
	
}
	
}

void Nloglikelihood(int nx, int M, double *lnL, double *x, double *mu, double *sqP, double *invsqP)
{ // Li = ln(N(xi|mu,P)), sqP is upper triangular after chol NOTE invsqP, is also upper triangular

int i,j,m;
double ncc, sumerr, sumerrinit, *err, cumsum;
err = (double *)malloc(nx*sizeof(double));//err[nx];


// Need the sum of the log of diagonal components of sqP for normalising constant component
ncc = 0.0;
for(i=0;i<nx;++i)
{
	ncc = ncc + log(fabs(sqP[i+nx*i]));
}

sumerrinit = nx*LOG2PI;
// Likelihood for each particle
for(m=0;m<M;++m)
{
	sumerr = sumerrinit;
	
	
	// calculate the error
	for(i=0;i<nx;++i)
	{
		err[i] = x[i+nx*m] - mu[i];
	}
	
	// err = invsqP*err
	// can overwrite because of UT nature
	for(i=0;i<nx;++i)
	{
        cumsum = 0.0;
		for(j=0;j<=i;++j)
		{

				cumsum = cumsum + err[j]*invsqP[j+nx*i];

		}
        // square the error and add it to a cumulative sum
        sumerr = sumerr + cumsum*cumsum;
	
	}
	

	sumerr = 0.5*sumerr;
	sumerr = sumerr + ncc; 
	lnL[m] = -sumerr;	
}

free(err);
}

void Nloglikelihood_no_NC(double NC, int nx, int M, double *lnL, double *x, double *mu, double *sqP, double *invsqP)
{ // Li = ln(N(xi|mu,P)), sqP is upper triangular after chol NOTE invsqP, is also upper triangular

int i,j,m;
double sumerr, sumerrinit, *err, cumsum;
err = (double *)malloc(nx*sizeof(double));//err[nx];


// Need the sum of the log of diagonal components of sqP for normalising constant component


sumerrinit = 0.0;
// Likelihood for each particle
for(m=0;m<M;++m)
{
	sumerr = sumerrinit;
	
	
	// calculate the error
	for(i=0;i<nx;++i)
	{
		err[i] = x[i+nx*m] - mu[i];
	}
	
	// err = invsqP*err
	// can overwrite because of UT nature
	for(i=0;i<nx;++i)
	{
        cumsum = 0.0;
		for(j=0;j<=i;++j)
		{

				cumsum = cumsum + err[j]*invsqP[j+nx*i];

		}
        // square the error and add it to a cumulative sum
        sumerr = sumerr + cumsum*cumsum;
	
	}
	

	sumerr = 0.5*sumerr;
// 	sumerr = sumerr; 
	lnL[m] = NC-sumerr;	
}

free(err);
}

/* The gateway function */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
/*

Apply one iteration of the Reweighting smoother to a GMM problem

 IN:
     lnw - Log-weights of filtered distribution at current timestep
     x - samples of filtered distribution at current timestep 
     lnws - Smoothed log-weights from next timestep
     xs - Smoothed samples from next time-step (For this method filtered samples=smoothed samples)
     u - Input vector
     PM - Process model structure, contains wm, a log weight vector
    
 OUT:
     lnws - Smoothed Log-weights */
	
// Get input pointers	

double *lnw    = mxGetPr(prhs[0]);
double *x    = mxGetPr(prhs[1]);
double *lnws    = mxGetPr(prhs[2]);
double *xs    = mxGetPr(prhs[3]);
double *u    = mxGetPr(prhs[4]);

mxArray *JMLS, *mxtmp, *modeli;
#define PM prhs[5]

double *A, *B, *sqQ, *invsqQ, *xp, *lnL, *wm;
int nz, k, l;


// Grab dims.
int M = mxGetN(prhs[1]);  // Number of particles in current timestep
int nx = mxGetM(prhs[1]);
int Mf = mxGetN(prhs[3]); // Number of particles in next timestep
int nu = mxGetM(prhs[4]);


	
mxtmp  = mxGetField(PM, 0, "N");
if (mxtmp!=NULL){
	nz = (int)(mxGetPr(mxtmp)[0]);
}
else {
	mexErrMsgTxt("No such N, in the PM structure");
}

mxtmp  = mxGetField(PM, 0, "A");
if (mxtmp!=NULL){
    A = mxGetPr(mxtmp);
}
else {
mexErrMsgTxt("No such A, in the PM structure");
}

    mxtmp  = mxGetField(PM, 0, "B");
if (mxtmp!=NULL){
    B = mxGetPr(mxtmp);
}
else {
mexErrMsgTxt("No such B, in the PM structure");
}

mxtmp  = mxGetField(PM, 0, "sqQ");
if (mxtmp!=NULL){
    sqQ = mxGetPr(mxtmp);
}
else {
mexErrMsgTxt("No such sqQ, in the PM structure");
}

mxtmp  = mxGetField(PM, 0, "invsqQ");
if (mxtmp!=NULL){
    invsqQ = mxGetPr(mxtmp);
}
else {
mexErrMsgTxt("No such invsqQ, in the PM structure");
}

mxtmp  = mxGetField(PM, 0, "wm");
if (mxtmp!=NULL){
    wm = mxGetPr(mxtmp);
}
else {
mexErrMsgTxt("No such wm, in the PM structure");
}

plhs[0] = mxCreateDoubleMatrix(M,1, mxREAL);
double *wT = mxGetPr(plhs[0]);

// Allocate space for the predictions
lnL   = (double *)malloc(nz*M*sizeof(double));
xp = (double *)malloc(nz*nx*M*sizeof(double));

// Precompute the normalising constants, save M log operations
// Precompute predictions
double *NC_arr = (double *)malloc(nz*sizeof(double));
int i;
for (l=0;l<nz;++l)
{
    double ncc = 0.0;
    for(i=0;i<nx;++i)
    {
        ncc = ncc + log(fabs(sqQ[i+nx*i+l*nx*nx]));
    }
    NC_arr[l] = wm[l] - ncc - 0.5*LOG2PI*((double) nx);//-0.5log(det(2*pi*Q^l))
    
    linearPrediction(nx, nu, M, &xp[nx*M*l], &A[nx*nx*l], x, &B[nx*nu*l], u);

}



for(k=0;k<Mf;k++)
{
    for (l=0;l<nz;++l)
    {
        Nloglikelihood_no_NC(NC_arr[l], nx, M, &lnL[l*M], &xp[nx*M*l], &xs[nx*k], &sqQ[nx*nx*l], &invsqQ[nx*nx*l]);
    }
    // LSE trick to collapse model likelihoods into a Mx1
    LSE_matrix(lnL, M, nz);
    
	for(l=0;l<M;++l)
	{
		lnL[l] = lnL[l] + lnw[l];
	}
    normalise_log_weights(lnL, M);
    
    for(l=0;l<M;++l)
	{
        lnL[l] = lnws[k]+lnL[l];
        if (k==0)
        {
            wT[l] = lnL[l];
        }
        else
        {
            // LSE trick
            wT[l] = LSE_pair(wT[l], lnL[l]);
            
        }
    }
    
	
}
        
free(lnL);
free(xp);
free(NC_arr);

//normalise_ln_weights(lnL)
normalise_log_weights(wT, M);

}