function wT = PS_logw_reweight(M,w,p,Mf,wf,pf,u,plant_params,PM) 

w = log(w);
wf = log(wf);
for k = 1:Mf
	lSum = nan(M,1);
	for l=1:PM.N
		A = PM.model(l).A;
		B = PM.model(l).B;
		Q = PM.model(l).Q;
		wm = PM.model(l).wm;
		
%                 for i =1:M
%                 
%                     [pfpi,~,~,Q] = PM.model(l).func(p(:,i),u,tk,plant_params);
%                     pfp(:,i) = pfpi;
%                 end
		
	   pfp = A*p + B*u;



		
		xe = repmat(pf(:,k),1,M) - pfp; 
		xe = chol(Q^-1) * xe;
		Dm2 = sum(xe.*xe,1)';
		if l ==1
			lSum = log(wm)-0.5*log(det(2*pi*Q))-0.5*Dm2';
		else
			lSum2 = [lSum; log(wm)-0.5*log(det(2*pi*Q))-0.5*Dm2'];		
			lSum = LSE(lSum2);
		end
	end
	if k ==1
		wT = wf(k) + normalise_log_weights(w'+lSum);
	else
		wT2 = [wT; wf(k) + normalise_log_weights(w'+lSum)];
		wT = LSE(wT2);
	end
end

wT = normalise_log_weights(wT');
wT=exp(wT);