function [xmin,xmax] = GM_plotrange(mu,P,plotSD)

[~,M] = size(mu);


for i =1:M
    maxi = mu(:,i) + diag(plotSD*P(:,:,i)^0.5); % ToDo: look into something that captures cross-covariance
    mini = mu(:,i) - diag(plotSD*P(:,:,i)^0.5);
    
    if (i==1)
        xmax = maxi;
        xmin = mini;
    else
        xmax = max(xmax,maxi);
        xmin = min(xmin,mini);
    end
end