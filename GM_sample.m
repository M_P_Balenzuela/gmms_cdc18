function x = GM_sample(n,mu,P,w)
%IN:
% n = number of samples to be drawn
% mu(:,i) = mean of i-th mode
% P(:,:,i) = covariance of i-th mode
% w(i) log weight of i-th mode

%OUT:
% x(:,i) = i-th sample

w = exp(w);
[nx,M] = size(mu);
modeIdx = randsample(M,n,true,w);

x = nan(nx,n);
j = 1;
for i = 1:M
    Mi = sum(i == modeIdx);
    if Mi > 0
        ej = j + Mi - 1;
        sqrtP = sqrtm(P(:,:,i));
        mui = mu(:,i);
        x(:,j:ej) = mui + sqrtP*randn(nx,Mi);
        j = j + Mi;
    end
end

% The built in GM sampling is slower, and has confusing documentation.
% obj = gmdistribution(mu',P,w'); % Documentation says sigma = covariance
% x = random(obj,n)';