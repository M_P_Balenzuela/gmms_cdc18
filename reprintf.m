function reprintf(text_str)
    persistent nstr
    if isempty(nstr)
        nstr = 0;
    end

    if nargin == 0
        nstr = 0;
        fprintf('\n');
    else
        for i = 1:nstr
            fprintf('\b');
        end
        fprintf('%s',text_str);
        nstr = length(text_str);
    end



