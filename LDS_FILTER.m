function [Nftp1,ftp1,Ftp1,wtp1] = LDS_FILTER(Nft,ft,Ft,wt,u,y,PM,MM, plant_params,KLReduction,dreduction,Mu)
%LOG WEIGHTS        [M,mu,P,w,Toff] = GMMF_matlab(M,mu,P,w,u,ym,t,PM,MM,plant_params,KLReduction)
nx = plant_params.Dimms.nx;

Nf = PM.N * MM.N* Nft;

ftp1 = nan(nx,Nf);
Ftp1 = nan(nx,nx,Nf);
wtp1 = nan(Nf,1);
z = nan(Nf,1);

s = 0;
for i = 1:PM.N
    PMi = PM.model(i);
    beta = PMi.wm;
    for j = 1:MM.N
        MMi = MM.model(j);
        gamma = MMi.wm;
        for l=1:Nft
            
            f = ft(:,l);
            F = Ft(:,:,l);
            
            s = s + 1;
            [fprime,Fprime,pprime] = LDS_FORWARD(f,F,y,PMi,MMi,u);
            ftp1(:,s) = fprime;
            Ftp1(:,:,s) = Fprime;
            wtp1(s) = log(pprime)+log(beta)+log(gamma) + wt(l);
            z(s) = (i-1)*MM.N + j; % discrete state 1:nz
            
        end
    end
end

wtp1 = normalise_log_weights(wtp1);

Nftp1 = s;
nz = MM.N*PM.N;

if (dreduction)

    [wtp1,ftp1,Ftp1,Nftp1] = discrete_reduction(wtp1,ftp1,Ftp1,z,nz,nx, Mu);
%     Nftp1 = nz;
else

[Nftp1, wtp1, ftp1, Ftp1] = KL_GMM_reduction_logw(KLReduction.Ml, KLReduction.Mu, KLReduction.lambda, Nftp1, wtp1, ftp1, Ftp1);

end
