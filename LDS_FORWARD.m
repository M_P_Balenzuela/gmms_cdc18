function [fprime,Fprime,pprime] = LDS_FORWARD(f,F,y,PM,MM,u)

% [~,A,B,Q] = PM.func(0,0,0,0);

A = PM.A;
B = PM.B;
Q = PM.Q;

% [~,C,D,R] = MM.func(0,0,0,0);

C = MM.C;
D = MM.D;
R = MM.R;


muh = A*f + B*u;
muv = C*muh + D*u;

sigmahh = A*F*A' + Q;
sigmavv = C*sigmahh*C'+R;
sigmavh = C*sigmahh;
fprime = muh + sigmavh'/sigmavv*(y-muv);
Fprime = sigmahh - sigmavh'/sigmavv*sigmavh;
pprime = exp(-0.5*(y-muv)'/sigmavv*(y-muv))*(det(2*pi*sigmavv))^-0.5;


end