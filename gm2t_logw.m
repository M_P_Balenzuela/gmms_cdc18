function Tidx = gm2t_logw(M, widx, muidx, Pidx, nx)

Tidx = nan(4,M);
for k = 1:M
    mu = muidx(:,k);
    P = Pidx(:,:,k);
    w = widx(k);
    
%     Tidx(1,k) = w*det(2*pi*P)^-0.5;
%     Tidx(2,k) = eye(nx)/P;
%     Tidx(3,k) = -eye(nx)/P*mu;
%     Tidx(4,k) = mu'/P*mu;
    
    % Numerical conditioning
%     logT1 = w + log(det(2*pi*P)^-0.5);
%     Tidx(4,k) = Tidx(4,k) - 2*log(Tidx(1,k));
%     Tidx(4,k) = Tidx(4,k) - 2*logT1;
    
    Tidx(4,k) = mu'/P*mu - 2*(log(det(2*pi*P)^-0.5) + w);
    Tidx(3,k) = -eye(nx)/P*mu;
    Tidx(2,k) = eye(nx)/P;
%     Tidx(1,k) = 1.0; % unused
end

end