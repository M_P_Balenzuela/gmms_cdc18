function [gprime,Gprime, lnlike] = LDS_BACKWARD(g,G,f,F,PMi,ukp1)

% [~,A,B,Q] = PMi.func(0,0,0,0);

A=PMi.A;
B=PMi.B;
Q = PMi.Q;

hbar = B*ukp1;

muh = A*f+hbar;
sigmahphp = A*F*A' + Q;
sigmahph = A*F;

arrowsigma = F - sigmahph'/sigmahphp*sigmahph;
arrowA = sigmahph'/sigmahphp;
arrowm = f-arrowA*muh;

gprime = arrowA*g + arrowm;
Gprime = arrowA*G*arrowA' + arrowsigma;

err = muh - g;

lnlike=-0.5*log(det(2*pi*sigmahphp))-0.5*err'/sigmahphp*err;