function [r,s] = normal_to_IF(mu,P)

r = P^(-1);
s = -r*mu;