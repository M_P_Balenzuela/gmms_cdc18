function [muidx,Pidx,widx] = t2gm_logw(Tidx, M, nx)

muidx = nan(nx,M);
Pidx = nan(nx,nx,M);
widx = nan(M,1);
for k = 1:M
    T2 = Tidx(2,k);
    T3 = Tidx(3,k);
    T4 = Tidx(4,k);

    P = eye(nx)/T2; 
    mu = -P*T3;
    widx(k) = 0.5*log(det(2*pi*P)) + (-0.5*(T4-mu'*T2*mu));
        
    muidx(:,k) = mu;
    Pidx(:,:,k) = P;
end               
 

widx = normalise_log_weights(widx);

end