function wT = PS_matlab_diffM_wrap(~,w,p,~,wf,pf,u,~,PM)


%GMM_reweight_PS_wrapper(PM, )

PMm.N = PM.N;

[nx,nu] = size(PM.model(1).B);

PMm.A = nan(nx,nx,PM.N);
PMm.B = nan(nx,nu,PM.N);
PMm.sqQ = nan(nx,nx,PM.N);
PMm.invsqQ = nan(nx,nx,PM.N);
PMm.wm = nan(PM.N,1);

for l=1:PM.N
    PMm.A(:,:,l) = PM.model(l).A;
    PMm.B(:,:,l) = PM.model(l).B;
    PMm.sqQ(:,:,l) = chol(PM.model(l).Q);
    PMm.invsqQ(:,:,l) = inv(PMm.sqQ(:,:,l));
    PMm.wm(l) = log(PM.model(l).wm);  
end


wT = GMM_reweight_PS_c(w,p,wf,pf,u,PMm);


