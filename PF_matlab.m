function [mu,w] = PF_matlab(u,ym,plant_params,M,w,mu,PM,MM)

% Unpacking
nx = plant_params.Dimms.nx;
ny = plant_params.Dimms.ny;
nu = plant_params.Dimms.nu;


    
    processModelWeights = zeros(PM.N,1);
    for j =1:PM.N
        processModelWeights(j) = PM.model(j).wm;
    end
    
    % Run PF

        % Prediction
            % Allocate space for prediction density
            mup = nan(nx, M);
            wp = nan(M,1);
            
            drawnsampleidx = randsample(M,M,true,exp(w));
            pmidx = randsample(PM.N,M,true,processModelWeights);
            
            randnstore = randn(nx,M);
            % Perform prediction
            for ki = 1:M
                k = pmidx(ki);
                % Get SS for PM mode
                A = PM.model(k).A;
                B = PM.model(k).B;
                Q = PM.model(k).Q;
                wm = PM.model(k).wm;
                % Get index range for this mode
                % Mean
                k = drawnsampleidx(ki);
                mup(:,ki) =  A*mu(:,k) + B*u +sqrtm(Q)*randnstore(:,ki);

                % Weights
                wp(ki,1) = -log(M);%  %w(k) + log(wm);              
            end
        
                
        % Correct weights using measurements
            measLike = nan(MM.N,M);
            for k = 1:MM.N
                % load measurement model parameters
                C = MM.model(k).C;
                D = MM.model(k).D;
                R = MM.model(k).R;                
                wm = MM.model(k).wm;
                % Predict measurement using MM
                muy = C*mup + D*u;
                
                Rinvsq = chol(R^-1);

                es = ym - muy;
                es = Rinvsq * es;
                es = es .* es;
                               
                ML = log(det(2*pi*R)) + sum(es,1);
                ML = -0.5*ML;
                measLike(k,:) = log(wm) + ML; %MM down, particles across
            end
            
            xstar = max(measLike);
            
            wf = wp + xstar' + log(sum( exp(measLike-repmat(xstar,MM.N,1)),1))';% log-sum-exp; 
            
            % Normalise the filtered distribution
            w = normalise_log_weights(wf);
            mu = mup;