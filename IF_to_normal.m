function [mu,P] = IF_to_normal(r,s)
P = r^(-1);
mu = -P*s;