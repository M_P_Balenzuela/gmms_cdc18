function [muf,Pf,lnwf,mufu, Pfu, priorkp1] = IMM_filter(JMLS,muin,Pin,lnwin, u, ym, priork)
% Perform one iteration of the IMM Filter

% IN:
	% JMLS - JMLS structure
	% muin(:,i) - Previous filtered mean for the i-th discrete state
	% Pin(:,:,i) - Previous filtered covariance for the i-th discrete state
	% lnwin(i) - Previous filtered log-weights for the i-th discrete state
	% u - Input vector
	% ym - Measurement vector
	
% OUT:
	% muf(:,i) - Filtered mean for the i-th discrete state
	% Pf(:,:,i) - Filtered covariance for the i-th discrete state
	% lnwf(i) - Filtered log-weight for the i-th discrete state 
	% mufu - Mean of the unimodal approximation of the filtered distribution
	% Pfu - Covariance of the unimodal approximation of the filtered distribution

    
    

    
% Get dimensions
nx = size(muin,1);
nz = JMLS.N;

% Update prior (used for smoother)
priorkp1 = JMLS.T + repmat(priork.',nz,1);

priorkp1 = LSE(priorkp1.').';
priorkp1 = normalise_log_weights(priorkp1);

% Calculate wij
wp = JMLS.T + repmat(lnwin',nz,1);

% Allocate space
mu = nan(nx,nz);
P = nan(nx,nx,nz);
w = nan(nz,1);
for i =1:nz
    wn = normalise_log_weights(wp(i,:)');
    [~, ~, muzf, Pzf] = GM_reduction_KL(wn, muin, Pin,1,1);
    mu(:,i) = muzf;
    P(:,:,i) = Pzf;
    w(i) = LSE(wp(i,:)');
end
% mu % uncomment to see reduction fromplem for the GMM class
w = normalise_log_weights(w);

muf = nan(nx,nz);
Pf = nan(nx,nx,nz);
lnwf = nan(nz,1);
for zf = 1:nz % next discrete state
    
    % Unpack model
    A = JMLS.model(zf).A;
    B = JMLS.model(zf).B;
    Q = JMLS.model(zf).Q;
    C = JMLS.model(zf).C;
    D = JMLS.model(zf).D;
    R = JMLS.model(zf).R;

    % KF prediction
    mup = A*mu(:,zf) + B*u;
    Pp = A*P(:,:,zf)*A' + Q;

    % KF correction
    es = ym - (C*mup + D*u); % Measurement Prediction error
    Py = C*Pp*C' + R;          
    Kg = Pp*C'/Py;
    muf(:,zf) = mup + Kg*es; 
    Pf(:,:,zf) = Pp - Kg*Py*Kg';
    logML = -0.5*log(det(2*pi*Py)) -0.5*es'/Py*es;

    % Calculate weights
    lnwf(zf) = w(zf) + logML;

end

% Reduce to one mode
lnwf = normalise_log_weights(lnwf);
% Reduce to one mode
[~, ~, mufu, Pfu] = GM_reduction_KL(lnwf, muf, Pf,1,1);
