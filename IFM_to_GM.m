function [muidx,Pidx,lnwidx] = IFM_to_GM(r,s,t)
% Convert a Information form mixture into a GM.

% IN:
    % r(:,:,i) - Information matrix of i-th component
    % s(:,i) - Information vector of i-th component
    % t(i) - Information weight of i-th component
    
% OUT:
    % lnwidx(i) - Log-weight of i-th component
    % muidx(:,i) - Mean of i-th component
    % Pidx(:,:,i) - Covariance of i-th component

[nx,M] = size(s);

muidx = nan(nx,M);
Pidx = nan(nx,nx,M);
lnwidx = nan(M,1);
for k = 1:M

    T2 = r(:,:,k); %Tidx(2,k); %r
    T3 = s(:,k); % Tidx(3,k); %s
    T4 = t(k); % Tidx(4,k); %t

    P = eye(nx)/T2; 
    mu = -P*T3;

    lnwidx(k) = 0.5*log(det(2*pi*P)) + (-0.5*(T4-mu'*T2*mu));
        
    muidx(:,k) = mu;
    Pidx(:,:,k) = P;
end               
 

lnwidx = normalise_log_weights(lnwidx);