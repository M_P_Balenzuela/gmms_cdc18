    function [wzred,muzred,Pzred,nummodes] = discrete_reduction(win,muin,Pin,zin,nz,nx,M)
    % nz is 1 indexing
    %log weights
    % reduce to one mode per discrete state
    
    wzred = nan(nz*M,1);
    muzred = nan(nx,nz*M);
    Pzred = nan(nx,nx,nz*M);
    
    nummodes = 0;
    for i=1:nz
        idxs = find(i==zin);
        wz = win(idxs);
        muz = muin(:,idxs);
        Pz = Pin(:,:,idxs);
         
        [Mzr, wzr, muzr, Pzr] = KL_GMM_reduction_logw(1, M, 0, length(wz), wz, muz, Pz); 
        

        wzred(nummodes+1:nummodes+Mzr) = wzr;
        muzred(:,nummodes+1:nummodes+Mzr) = muzr;
        Pzred(:,:,nummodes+1:nummodes+Mzr) = Pzr;
        nummodes=nummodes+Mzr;
    end
    
    
wzred = wzred(1:nummodes);
muzred = muzred(:,1:nummodes);
Pzred = Pzred(:,:,1:nummodes);