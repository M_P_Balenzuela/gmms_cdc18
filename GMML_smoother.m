function [Mbifp,rbifp,sbifp,tbifp,Ms,ws,mus,Ps] = GMML_smoother(wf,muf,Pf,rbif,sbif,tbif,MM,PM,u,ym)
% Perform one iteration of the GMML smoother

% IN:
    % wf(i) - Log-weight of the i-th filtered mode
    % muf(:,i) - Mean of the i-th filtered mode
    % Pf(:,:,i) - Covariance of the i-th filtered mode
    % rbif(:,:,j) - Information matrix for the j-th BIF mode
    % sbif(:,j) - Information vector for the j-th BIF mode
    % tbif(j) - Information weight for the j-th BIF mode
    % MM - Measurement model structure
    % PM - Process model structure
    % u - Input vector
    % ym - Measurement vector
    
% OUT:
    % Mbifp - Number of modes in the BIF likelihood
    % rbifp(:,:,i) - Information matrix for the i-th BIF mode
    % sbifp(:,:,i) - Information vector for the i-th BIF mode
    % tbifp(:,:,i) - Information weight for the i-th BIF mode
    % Ms - Number of smoothed modes in GM
    % ws(j) - Log-weight for the j-th smoothed mode
    % mus(:,j) - Mean of the j-th smoothed mode
    % Ps(:,:,j) - Covariance of the j-th smoothed mode
    
[nx,Mbif] = size(sbif);

% Update the BIF with measurements
    % Allocate space
    Mtbifu = Mbif * MM.N;

    rbifu = nan(nx,nx,Mtbifu);
    sbifu = nan(nx,Mtbifu);
    tbifu = nan(Mtbifu,1);
    
    idx = 0;
    for k=1:MM.N
        C = MM.model(k).C;
        D = MM.model(k).D;
        R = MM.model(k).R;                
        wm = MM.model(k).wm;

        lognc = -0.5*log(det(2*pi*R));
        Du = D*u;
        for l=1:Mbif
            idx = idx + 1;

            % Update the information matrix
            rbifu(:,:,idx) = rbif(:,:,l) + C'/R*C;
            
            % Update the information vector
            sbifu(:,idx) = sbif(:,l) + C'/R*(Du-ym);

            % Update information weight
            logtbif1= lognc + log(wm);
            tbifu(idx) = tbif(l) + (Du-ym)'/R*(Du-ym) - 2*logtbif1; 

        end
    end

    
    
% Propagate density back in time using PM's
    % Allocate space
    Mbifp = Mtbifu * PM.N;
    rbifp = nan(nx,nx,Mbifp);
    sbifp = nan(nx,Mbifp);
    tbifp = nan(Mbifp,1);    
    
    idx = 0;
    for k = 1:PM.N
        A = PM.model(k).A;
        B = PM.model(k).B;
        Q = PM.model(k).Q;
        invQ  = eye(nx)/Q;
        wm = PM.model(k).wm;

        lognc = -0.5*log(det(2*pi*Q));
        Bu = B*u;

        for l = 1:Mtbifu
            idx = idx+1;
            
            % Grab previous information variables
            rbiful = rbifu(:,:,l);
            sbiful = sbifu(:,l); 
            tbiful = tbifu(l);
            
            O = eye(nx)/(rbiful + eye(nx)/Q)';

            % Information matrix
            rbifp(:,:,idx) = A'*invQ*(A - O*invQ*A);
            % Information vector
            sbifp(:,idx) = A'*invQ*(Bu+O*sbiful-O*invQ*Bu);
            
            % Information weight
            t2pv2 = rbiful+invQ;
            logTbif1 = 0.5*log(det(2*pi*eye(nx)/t2pv2)) + lognc + log(wm);
            logTbif2 = tbiful + Bu'*invQ*Bu + sbiful'*O*(-sbiful+invQ*Bu)+Bu'*invQ*O*sbiful-Bu'*invQ*O*invQ*Bu;
            
            tbifp(idx) = logTbif2 - 2*logTbif1;

        end
    end


% Reduction no longer included.
% % Reduce the number of modes (if possible)
% % 
% % if (0)
% %     if (ji < N+1-nx)
% %         for k=1:Mbif
% %             if abs(det(Tbif(2,k))) < 10^-4
% %                 disp('Warning: GMM equivilant may not exist');
% %             end
% %         end
% %         % Convert to GM
% %         [mubif,Pbif,wbif] = t2gm_logw(Tbif, Mbif, nx);
% % 
% %         % KL reduction
% %         [Mbif, wbif, mubif, Pbif] = KL_GMM_reduction_logw(KLReduction.Ml, KLReduction.Mu, KLReduction.lambda, Mbif, wbif, mubif, Pbif);
% % 
% %         % Convert back to working distribution
% %         Tbif = gm2t_logw(Mbif, wbif, mubif, Pbif, nx);
% % 
% %     end    
% % 
% % else
% % 
% %     Tbif = Tbif + Toff;
% %     [mubif,Pbif,wbif] = t2gm_logw(Tbif, Mbif, nx);
% % 
% %     % KL reduction
% %     [Mbif, wbif, mubif, Pbif] = KL_GMM_reduction_logw(KLReduction.Ml, KLReduction.Mu, KLReduction.lambda, Mbif, wbif, mubif, Pbif);
% % 
% %     % Convert back to working distribution
% %     Tbif = gm2t_logw(Mbif, wbif, mubif, Pbif, nx);
% % 
% %     Tbif = Tbif - Toff;
% % 
% % 
% % end


% Merge with filtered density
    % Unpackage density



    % Convert filtered densitity into information form
    [rf,sf,tf] = GM_to_IFM(wf, muf, Pf);

    % Combine the two densities
    [Ms, rsm, ssm, tsm] = IFM_combiner(rf,sf,tf, rbifp,sbifp,tbifp);
   
    % Convert the density back into GM
    [mus,Ps,ws] = IFM_to_GM(rsm,ssm,tsm);
  
    % Normalise the smoothed distribution        (included in former function)      
%     ws = normalise_log_weights(ws);