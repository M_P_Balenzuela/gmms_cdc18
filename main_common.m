%% KL Params
KLReduction.Ml = 1;
KLReduction.Mu = 8;%4;%10;
KLReduction.lambda = 10^-20;

Mud = 2; % for aSLDS EC

%% Define common parameters
N = 30; % Number of time steps filtering will be completed for
T = 0.005; % Time step - 200Hz
nx = 1; % Number of states
nu = 1; % Number of inputs
ny = 1; % Number of measurements
x0 = 0.0; % True x0

%% Define plant parameters
plant_params.Dimms.nx = nx;
plant_params.Dimms.ny = ny;
plant_params.Dimms.nu = nu;
plant_params.Dimms.N = N;

%% Define models
% Define PM's

    PM.N = 2; % Number of PM's

    PM.model(1).wm = 0.5;
    PM.model(1).A = 1;
    PM.model(1).B = 0.0;
    PM.model(1).Q = 0.01;
    
    PM.model(2).wm = 1 - PM.model(1).wm;
    PM.model(2).A = -1;
    PM.model(2).B = -sqrt(0.01)/2;
    PM.model(2).Q = 0.01;%15;%0.02;
    
    
% Define MM's

MM.N = 2; % Number of MM's

    MM.model(1).wm = 0.5;
    MM.model(1).C = 1;
    MM.model(1).D = sqrt(0.18);
    MM.model(1).R = 0.18;%15;

    MM.model(2).wm = 1-MM.model(1).wm;   
    MM.model(2).C = -1.0;
    MM.model(2).D = -sqrt(0.18);
    MM.model(2).R = 0.15/2*1.2;   
    
    JMLS = GMM_to_JMLS(PM,MM);
 
    
%% Check model validity and get cumulative sum of weights

wPMs = zeros(PM.N,1);
sumw = 0;
for i = 1:PM.N
    sumw = sumw + PM.model(i).wm;
    wPMs(i) = sumw;
end
if (sumw ~= 1.0)
   error('Error: The process model weights do not sum to 1.0'); 
end

wMMs = zeros(MM.N,1);
sumw = 0;
for i = 1:MM.N
    sumw = sumw + MM.model(i).wm;
    wMMs(i) = sumw;
end
if (sumw ~= 1.0)
   error('Error: The measurement model weights do not sum to 1.0'); 
end

%% Prior

M0=1;
w0=0; % as of now this is a log weight
P0(1,1,1)=0.5;
mu0=x0+ sqrt(P0);

% M0 = 2;
% w0 = log([0.5 0.5]);
% P0(1,1,1)=0.01;
% P0(1,1,2)=0.01;
% mu0 = [x0 -x0];


%% Generate data
% Input vector
uidx = ones(1,N);%sin((1:N)/10*2*pi);%randn(nu,N);
% uidx = ones(size(uidx));

% Allocate some space for true states and measurements
xidx = nan(nx,N);
yidx = nan(ny,N);
% tidx = nan(1,N);

% Run the  true model
x = x0;
MMmodestore = nan(N,1);
PMmodestore = nan(N,1);
tk = 0;
for i = 1:N
    % Get input for this timestep
    u = uidx(:,i);
    
    % Decide which modes to draw from
    PMmode = discreteDraw(wPMs);
    MMmode = discreteDraw(wMMs);
    MMmodestore(i) = MMmode;
    PMmodestore(i) = PMmode;
    
    % Get the true linear SS system
        %PM


        
        A = PM.model(PMmode).A;
        B = PM.model(PMmode).B;
        Q = PM.model(PMmode).Q;
        
        C = MM.model(MMmode).C;
        D = MM.model(MMmode).D;
        R = MM.model(MMmode).R;
        
        v = sqrtm(Q)*randn(nx,1);
        n = sqrtm(R)*randn(ny,1);

    % Run SS
    x = A*x + B*u + v;
    xidx(:,i) = x;
    yidx(:,i) = C*x + D*u + n;    
    

end


%% GMMF / GMMS
  tic;
    % Setup GMMF
    mu = mu0;
    P = P0;
    w = w0;
    M = M0;
    
    % Run GMMF
    for j = 1:N
        u = uidx(:,j);
        ym = yidx(:,j);
     
        [M,mu,P,w,Toff] = GMMF_matlab(M,mu,P,w,u,ym,PM,MM,plant_params,KLReduction);

        % Save Filtered density
        GMMF.den(j).M = M;
        GMMF.den(j).mu = mu;
        GMMF.den(j).P = P;
        GMMF.den(j).w = w;
        GMMF.Toff(j).T = Toff;
        
        
    end
    
    tGMMF = toc
    
  %%  
    tic;
    % 2-Filter GMM Smoothing 
        % Save density at k=N
        GMMS.den(N).M = GMMF.den(N).M;
        GMMS.den(N).mu = GMMF.den(N).mu; 
        GMMS.den(N).w = GMMF.den(N).w;
        GMMS.den(N).P = GMMF.den(N).P;

        % uniform prior on BIF likelihood
        rbif = zeros(nx);
        sbif = zeros(nx,1);
        tbif = 0;
        
%         Mbif = 1;
        % Run the 2-Filter smoother
        for j = N-1:-1:1
            u = uidx(:,j+1);
            ym = yidx(:,j+1); 
            

            wf = GMMF.den(j).w; 
            muf = GMMF.den(j).mu;
            Pf = GMMF.den(j).P;
            
            % Run the smoother
            [~,rbif,sbif,tbif,~,ws,mus,Ps] = GMML_smoother(wf,muf,Pf,rbif,sbif,tbif,MM,PM,u,ym);
            
            % Reduce the smoothed mixture
            [Ms, ws, mus, Ps] = GM_reduction_KL(ws, mus, Ps, KLReduction.Mu, KLReduction.Ml, KLReduction.lambda);
            
            if j <= N-nx
               % Reduce the BIF likelihood
               [mubif,Pbif,lnwbif] = IFM_to_GM(rbif,sbif,tbif);    
               [Mbif, lnwbif, mubif, Pbif] = GM_reduction_KL(lnwbif, mubif, Pbif, KLReduction.Mu, KLReduction.Ml, KLReduction.lambda);
               [rbif,sbif,tbif] = GM_to_IFM(lnwbif, mubif, Pbif);
            end
            
            % Store the Smoothed distribution
            GMMS.den(j).M = Ms;
            GMMS.den(j).mu = mus;
            GMMS.den(j).P = Ps;
            GMMS.den(j).w = ws;

        end    
    
    tGMMS=toc
    
    % check for numeric problems
%     GMMS.den(:).w
for j=1:N
    if any(isnan(GMMS.den(j).w))%inf weights allowed with log-w  | isinf(GMMS.den(j).w)
        error('Error: Smoothed weights are NaN');
    end
end




%% GPB2F / GPB2S
    
 tic;
    % Setup GPB2 filter
    nz = JMLS.N;
    mu = repmat(mu0(:,1),1,nz);
    P = repmat(P0(:,:,1),1,1,nz);
    w = repmat(log(1/nz),nz,1);

    % Run GPB2 filter
    for j = 1:N
        u = uidx(:,j);
        ym = yidx(:,j);
        
        [mu,P,w,~,~] = GPB2_FILTER(JMLS,mu,P,w, u, ym, plant_params);
        
        % Save Filtered density
        GPB2f.den(j).mu = mu;
        GPB2f.den(j).P = P;
        GPB2f.den(j).w = w;        
    end
    
tGPB2F = toc

     
    
tic;
        % Save density at k=N
        mus = GPB2f.den(N).mu;
        GPB2s.den(N).mu = mus;
        
        ws = GPB2f.den(N).w;
        GPB2s.den(N).w = ws;
        
        Ps = GPB2f.den(N).P;
        GPB2s.den(N).P = Ps;
    

        % Run the GPB2 smoother
        for j = N-1:-1:1
           
            % Run the BIF
            u = uidx(:,j+1);
            
            % Unpackage density
            muf = GPB2f.den(j).mu;
            Pf = GPB2f.den(j).P;
            wf = GPB2f.den(j).w;  
            
            [mus,Ps,ws] = GPB2_SMOOTHER(JMLS,mus,Ps,ws,muf,Pf,wf,u,plant_params);

            % Store the Smoothed distribution
            GPB2s.den(j).mu = mus;
            GPB2s.den(j).P = Ps;
            GPB2s.den(j).w = ws;
            


        end    
    
tGPB2S=toc

%% IMMF / IMMS
   
tic;
    % Setup IMM filter
    nz = JMLS.N;
    mu = repmat(mu0(:,1),1,nz);
    P = repmat(P0(:,:,1),1,1,nz);
    w = repmat(log(1/nz),nz,1);

    prior = w;
    % Run IMM filter
    for j = 1:N
        u = uidx(:,j);
        ym = yidx(:,j);
        
        [mu,P,w,~,~, prior] = IMM_filter(JMLS,mu,P,w, u, ym, prior);
        
        % Save Filtered density
        IMMF.den(j).prior = prior;
        IMMF.den(j).mu = mu;
        IMMF.den(j).P = P;
        IMMF.den(j).w = w;       
        IMMF.den(j).d = (1:nz).';
    end
    
tIMMF = toc
   
     tic;
        % Save density at k=N
        IMMS.den(N).mu = IMMF.den(N).mu; 
        IMMS.den(N).w = IMMF.den(N).w;
        IMMS.den(N).P = IMMF.den(N).P;
        IMMS.den(N).d = IMMF.den(N).d;
        
        r = nan(nx,nx,nz);
        s = nan(nx,nz);
        wbif = ones(nz,1)*log(1/nz);
        u = uidx(:,N);
        ym = yidx(:,N);
        for j =1:nz
            C = JMLS.model(j).C;
            D = JMLS.model(j).D;
            R = JMLS.model(j).R;
            invR = R^(-1);
            r(:,:,j) = C'*invR*C;
            s(:,j) = C'*invR*(D*u - ym);
        end
        
        
        % Run the IMM smoother
        for j = N-1:-1:1
            % Run the BIF
            u = uidx(:,j+1);
            upr = uidx(:,j);
            ympr = yidx(:,j); 
            
            muf = IMMF.den(j).mu;
            Pf = IMMF.den(j).P;
            wf = IMMF.den(j).w;   
            prior = IMMF.den(j).prior;
            
            [mus,Ps,ws, ~,~, r,s,wbif] = IMM_smoother(JMLS,r,s,wbif,muf,Pf,wf,ympr,u,upr, prior);

            % Store the Smoothed distribution
            IMMS.den(j).mu = mus;
            IMMS.den(j).P = Ps;
            IMMS.den(j).w = ws;
            IMMS.den(j).d = (1:nz).';

        end    
    
tIMMS=toc

%% Barber EC
    
tic;
    % Setup LDS_FILTER
    mu = mu0;
    P = P0;
    w = w0;
    M = M0;
    
    % Run LDS_FILTER
    for j = 1:N
        u = uidx(:,j);
        ym = yidx(:,j);
        
        dred = 1;
        [M,mu,P,w] = LDS_FILTER(M,mu,P,w,u,ym,PM,MM,plant_params,KLReduction, dred,Mud);
        
        % Save Filtered density
        aSLDSf.den(j).M = M;
        aSLDSf.den(j).mu = mu;
        aSLDSf.den(j).P = P;
        aSLDSf.den(j).w = w;     
        
    end
    

        
    taSLDSfilter = toc
    
    %%%%%%%%%%%%%%
    tic;
        % Save density at k=N
        Ms = aSLDSf.den(N).M;
        aSLDS.den(N).M = Ms;
        
        mus = aSLDSf.den(N).mu;
        aSLDS.den(N).mu = mus;
        
        ws = aSLDSf.den(N).w;
        aSLDS.den(N).w = ws;
        
        Ps = aSLDSf.den(N).P;
        aSLDS.den(N).P = Ps;
    

        % Run the aSLDS smoother
        for j = N-1:-1:1
           
            % Run the BIF
            u = uidx(:,j+1);

            
            % Unpackage density
            Mf = aSLDSf.den(j).M;
            muf = aSLDSf.den(j).mu;
            Pf = aSLDSf.den(j).P;
            wf = aSLDSf.den(j).w;  
            
                       
            [Ms, ws, mus, Ps] = LDS_SMOOTHER(muf,Pf,wf,Mf,mus,Ps,ws,Ms,PM,u,plant_params, 1, Mud); % 1 is to seperate from GPBS
            

            % Store the Smoothed distribution
            aSLDS.den(j).M = Ms;
            aSLDS.den(j).mu = mus;
            aSLDS.den(j).P = Ps;
            aSLDS.den(j).w = ws;
            


        end    
    
    taSLDSsmoother=toc
    
    % check for numeric problems
%     GMMS.den(:).w
for j=1:N
    if any(isnan(aSLDS.den(j).w))%inf weights allowed with log-w  | isinf(GMMS.den(j).w)
        error('Error: Smoothed weights are NaN');
    end
end


%% PF / Reweight PS
    M=M_PF_NUMBER;
tic;

    % Draw sample from GM prior
    mu = GM_sample(M,mu0,P0,w0);
    PF_PRIOR = mu;
    w = repmat(-log(M),M,1);
    
   for j = 1:N
%         disp(['Filtered j=' num2str(j)]);
        u = uidx(:,j);
        ym = yidx(:,j);

        
        [mu,w] = PF_matlab(u,ym,plant_params,M,w,mu,PM,MM);
        
        % Save Filtered density
        PF.den(j).mu = mu;
        PF.den(j).w = w;
        
        
   end
tPF = toc
   
tic;
    pf = PF.den(N).mu;
    wf = PF.den(N).w;
    PS.den(N).mu = pf;%  Samples remain in the same spot
    PS.den(N).w = wf;
    for j=N-1:-1:1
%         disp(['Smoothed j=' num2str(j)]);
        
        u = uidx(:,j+1);
        p = PF.den(j).mu;
        w = PF.den(j).w;        
        
       
        reprintf(['PS processing k=' num2str(j)]);
        wT = PS_matlab_diffM_wrap(M,w,p,M,wf,pf,u,plant_params,PM) ;
        

        
        %save density
%         PS.den(j).mu = pf; Samples remain in the same spot
        PS.den(j).w = wT;
        PS.den(j).mu = PF.den(j).mu;
        % new becomes old
        pf=p;
        wf=wT;

    end
    reprintf('PS completed!');
    reprintf();
    
tPS = toc
    