function [sidx,eidx] = ith_block(k,N)
% Get index range for k kth block of N elements

sidx = (k-1)*N+1;
eidx = k*N;


end