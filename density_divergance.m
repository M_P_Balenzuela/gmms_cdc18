function [TVD,KL,sKL] = density_divergance(P, Q)

TVD = 0.5*sum(abs(P-Q));
KL = sum(P.*log((P+eps)./(Q+eps)));
sKL = KL + sum(Q.*log((Q+eps)./(P+eps)));

