function wn = normalise_log_weights(w)

% Using the log-sum-exp trick:
%https://hips.seas.harvard.edu/blog/2013/01/09/computing-log-sum-exp/

% Find max value
w = real(w); % for robustness.
wmax = max(w);
wn = w-wmax-log(sum(exp(w-wmax)));


end