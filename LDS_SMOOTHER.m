function [Ns, wstp1, stp1, Stp1] = LDS_SMOOTHER(ft,Ft,wft,Nft,gt,Gt,wgt,Ngt,PM,u, plant_params, EC,  Mud)

% assumes dreduction used for filtering density


nx = plant_params.Dimms.nx;

Ns = PM.N * Nft * Ngt;

stp1 = nan(nx,Ns);
Stp1 = nan(nx,nx,Ns);
wstp1 = nan(Ns,1);
z = nan(Ns,1);

s = 0;
for m=1:Ngt
    g = gt(:,m);
    G = Gt(:,:,m);
    for i = 1:PM.N
        PMi = PM.model(i);
        beta = PMi.wm;
        for l = 1:Nft
            f = ft(:,l);
            F = Ft(:,:,l);
            s = s+1;
            
            [gprime,Gprime,lnlike] = LDS_BACKWARD(g,G,f,F,PMi,u);
            
            stp1(:,s) = gprime;
            Stp1(:,:,s) = Gprime;
            z(s) = l;
            wstp1(s) = log(beta) +  wft(l) + lnlike*EC;
            
            if ~EC
                if m >= 3
                    NgtPM = 2;
                else
                    NgtPM = 1;
                end
                
                if i ~= NgtPM
                    wstp1(s) = -inf;
                %(l-1)*PM.N + i; % discrete state 1:nz
                end
            end
           
        end
    end
end

% Perhaps the fewest lines of code needed for a reweighting smoother
% implementation ever
for m=1:Ngt
    [sidx,eidx] = ith_block(m,Nft*PM.N);
    wstp1(sidx:eidx)=normalise_log_weights(wstp1(sidx:eidx)) + wgt(m);
end



nz = Nft;

wstp1 = normalise_log_weights(wstp1);

[wstp1,stp1,Stp1,Ns] = discrete_reduction(wstp1,stp1,Stp1,z,nz,nx, Mud);

