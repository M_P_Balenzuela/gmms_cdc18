function idx = discreteDraw(w)
% takes in cumulative sum of weights and returns an idex chosen

if(length(w) == 1)
    idx = 1;
else
    rn = rand();
    idx = sum(rn > w) + 1;
%     if rn <0.7
%         idx=1;
%     else
%         idx=2;
%     end
end



end
