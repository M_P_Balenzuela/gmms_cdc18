function L = normal_loglikelihood(y,mu,P)

es = y-mu;
L = -0.5*log(det(2*pi*P)) -0.5*es'/P*es;