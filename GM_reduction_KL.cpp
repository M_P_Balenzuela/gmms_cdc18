#include "mex.h"
#include "math.h"
#include <stdlib.h>

#include "Eigen\Dense"
using namespace Eigen;


/*
double get_max(double a, double b)
{
	if (a>b)
	{
		return(a)
	}
	else
	{
		return(b);
	}
}*/

double LSE_pair(double a, double b)
{
	if ((a==-mxGetInf())&&(b==-mxGetInf())) // to prevent to appearence of nans
	{
		return(a);
	}
	
    if (a<b)
    {
        // swap a and b
        double c = a;
        a = b;
        b = c;
    }
	// assuming a is larger
	b = b-a;
	b = exp(b);
	b = b + 1.0;
	b = log(b);
	a = a + b;
	return(a);        
        
}

double get_UB_KL(int nx, double lnwi, double *mui, double *Pi, double lnwj, double *muj, double *Pj)
{
	double lnwij = LSE_pair(lnwi, lnwj);
	
	double wiij=exp(lnwi-lnwij);
	double wjij=exp(lnwj-lnwij);
	double wijij = exp(lnwi+lnwj-2*lnwij);
	
	// use eigen here
	Map<MatrixXd> emui(mui, nx, 1);
	Map<MatrixXd> emuj(muj, nx, 1);
	Map<MatrixXd> ePi(Pi, nx, nx);
	Map<MatrixXd> ePj(Pj, nx, nx);
	
	MatrixXd mudiff = emui - emuj;
	MatrixXd Pij = wiij*ePi + wjij*ePj+wijij*mudiff*mudiff.transpose();
	
	double B = exp(lnwij)*log(Pij.determinant()) - exp(lnwi)*log(ePi.determinant()) - exp(lnwj)*log(ePj.determinant());
	
	return(0.5*B); // 0.5 effects lambda threshold	
	
}

// Overwrite i-th mode with merged result
void N_mode_merge(int nx, double *lnwip, double *mui, double *Pi, double *lnwjp, double *muj, double *Pj)
{
	double lnwi = *lnwip;
	double lnwj = *lnwjp;
	
	
	double lnwij = LSE_pair(lnwi, lnwj);
	double wiij=exp(lnwi-lnwij);
	double wjij=exp(lnwj-lnwij);
	double wijij = exp(lnwi+lnwj-2*lnwij);
	
	//mexPrintf("wi=%lf,wj=%lf, wij=%lf, wiij=%lf, wjij=%lf, wijij=%lf\n",lnwi,lnwj,lnwij, wiij, wjij, wijij);
	
	// use eigen here
	Map<MatrixXd> emui(mui, nx, 1);
	Map<MatrixXd> emuj(muj, nx, 1);
	Map<MatrixXd> ePi(Pi, nx, nx);
	Map<MatrixXd> ePj(Pj, nx, nx);
	
	MatrixXd mudiff = emui - emuj;
	MatrixXd Pij = wiij*ePi + wjij*ePj+wijij*mudiff*mudiff.transpose();
	
	MatrixXd muij = wiij*emui + wjij*emuj;
	
	// copy lnwij, muij, and Pij into i-th slot
	*lnwip = lnwij;
	memcpy(mui, muij.data(), nx*sizeof(double));
	memcpy(Pi, Pij.data(), nx*nx*sizeof(double));
	// seems wasteful that is was almost calculated previously (but atleast we don't need storage for all possible merges)
	//double B = exp(lnwij)*log(Pij.determinant()) - exp(lnwi)*log(ePi.determinant()) - exp(lnwj)*log(ePj.determinant());
	//return(0.5*B);
	
}

// Overwrite i-th mode with the j-th one
void mode_overwrite(int nx, double *lnwi, double *mui, double *Pi, double *lnwj, double *muj, double *Pj)
{
	*lnwi = *lnwj;
	memcpy(mui, muj, nx*sizeof(double));
	memcpy(Pi, Pj, nx*nx*sizeof(double));
}




double find_lowest_UT_Mat(double *B, int M, int k, int *idxi, int *idxj)
{
    int i,j;
	// Set first element as max
    *idxi = 0;
	*idxj = 1;
    double lowest = B[M];
    for (j=2; j<k; ++j)
    {
		for (i=0; i<j; ++i)
		{
			if (lowest>B[i+M*j])
			{
				lowest = B[i+M*j];
				*idxi = i;
				*idxj = j;
			}
		}
    }
	
	return(lowest);
    
}


// Perform KL reduction on a GM mixture, return the number of modes in the reduced mixture
int GM_KL_reduction(int nx, int M, double *w_arr, double *mu_arr, double *P_arr, int Mu, int Ml, double Lambda)
{
	double *BM; // Pointer to B(i,j) matrix
	int i, j, x;
	
	double *mui, *muj, *mux, *Pi, *Pj, *Px;
	
	int nx2 = nx*nx;
	
	int k = M;
	
	// First thing is to remove any elements with zero weight 
	j = 0;
	while ((j<k)&&(k>Ml))//(k!=0))
	{
		if (w_arr[j]==-mxGetInf())
		{
			// swap j with last
			--k;
			mui = &mu_arr[nx*k];
			Pi = &P_arr[nx2*k];
			muj = &mu_arr[nx*j];
			Pj = &P_arr[nx2*j];
			mode_overwrite(nx, &w_arr[j], muj, Pj, &w_arr[k], mui, Pi);
		}
		else
		{
			++j;
		}
			
	}
	
	M = k;
	
	BM = (double *)malloc(M*M*sizeof(double));
	for (j=1; j<M; ++j) // Fill the UT part (not the diagonal)
	{
		muj = &mu_arr[nx*j];
		Pj = &P_arr[nx2*j];
		for (i=0; i<j; ++i)
		{
			mui = &mu_arr[nx*i];
			Pi = &P_arr[nx2*i];
			BM[i+j*M] = get_UB_KL(nx, w_arr[i], mui, Pi, w_arr[j], muj, Pj);
		}
	}
	
	
	
	double Bmin = find_lowest_UT_Mat(BM, M,k, &i, &j);
	while ((k>Mu)||((k>Ml)&&(Bmin <= Lambda)))
	{
		// merge (i,j) into i 
		muj = &mu_arr[nx*j];
		Pj = &P_arr[nx2*j];
		mui = &mu_arr[nx*i];
		Pi = &P_arr[nx2*i];
		// Merge the pair with the lowest KL divergence value 
		N_mode_merge(nx, &w_arr[i], mui, Pi, &w_arr[j], muj, Pj);
		
		// Update i-th mode elements of B matrix
		for (x=0;x<k;++x)
		{
			if (x != j)
			{
				if (x<i) // B(x,i) (upper diagonal)
				{
					mux = &mu_arr[nx*x];
					Px = &P_arr[nx2*x];
					BM[x+i*M] = get_UB_KL(nx, w_arr[i], mui, Pi, w_arr[x], mux, Px);
				}
				if (x>i) // B(i,x) (upper diagonal)
				{
					mux = &mu_arr[nx*x];
					Px = &P_arr[nx2*x];
					BM[i+x*M] = get_UB_KL(nx, w_arr[i], mui, Pi, w_arr[x], mux, Px);				
				}
			}
		
		}
		// Write over j with the last and reduce the mode count
		--k;
		mui = &mu_arr[nx*k];
		Pi = &P_arr[nx2*k];
		mode_overwrite(nx, &w_arr[j], muj, Pj, &w_arr[k], mui, Pi);

		// Update B matrix
		int kM = k*M;
		for (x=0;x<k;++x)
		{
			if (x<j) // B(x,j)
			{
				BM[x+j*M] = BM[x+kM];
			}

			if (x>j) // B(j,x)
			{
				BM[j+x*M] = BM[x+kM];
			}			
		}	

		// Find new pair and KL Upper bound
		Bmin = find_lowest_UT_Mat(BM, M, k, &i, &j);
	}
	
	// Free Malloc-ed space
	free(BM);
	
	// Return new count
	return(k);
}


/* The gateway function */
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
/*
function [k, lnw, u, P] = GM_reduction_KL_matlab(lnw, u, P, Mu, Ml, lambda)
% Apply Kullback-Leibler reduction to a GM

% IN:
	% lnw(i) - Log-weight of i-th mode
	% u(:,i) - Mean of i-th mode
	% P(:,:,i) - Covariance of i-th mode
	% Mu -Maximum number of components in reduced mixture

% OPTIONAL INPUTS:
	% Ml - Minimum number of components after reduction (may be fewer if input
	% GM has less components)
	% lambda - Error threshold, used for deturmining if mixture should have
	% fewer than Mu elements

% OUT:
	% k - Number of components in reduced GM
	% lnw(i) - Log-weight of i-th mode in reduced GM
	% u(:,i) - Mean of i-th mode in reduced GM
	% P(:,:,i) - Covariance of i-th mode in reduced GM */
	
// Get input pointers	
double *lnw = mxGetPr(prhs[0]);
double *mu = mxGetPr(prhs[1]);
int M = mxGetN(prhs[1]); // Number of initial modes
int nx = mxGetM(prhs[1]); // get state dimension
double *P = mxGetPr(prhs[2]);
int  Mu = (int) *mxGetPr(prhs[3]);

int Ml;
double Lambda;

if (nrhs < 5)
{
	Ml = 0;
}
else
{
	Ml = (int) *mxGetPr(prhs[4]);
}

if (nrhs < 6)
{
	Lambda = 0.0;
}
else
{
	Lambda = (double) *mxGetPr(prhs[5]);
}


int nx2 = nx*nx;
int nxM = nx*M;
int nx2M = nxM*nx;
// copy the initial mixture
double *w_arr, *mu_arr, *P_arr;
w_arr = (double *)malloc(M*sizeof(double));
mu_arr = (double *)malloc(nxM*sizeof(double));
P_arr = (double *)malloc(nx2M*sizeof(double));

memcpy(w_arr, lnw, M*sizeof(double));
memcpy(mu_arr, mu, nxM*sizeof(double));
memcpy(P_arr, P, nx2M*sizeof(double));

// Do some KL reduction
int k = GM_KL_reduction(nx, M, w_arr, mu_arr, P_arr, Mu, Ml, Lambda);

// Pointers to output space
double *kp, *lnw_o, *mu_o, *P_o;
// Copy the first k modes into the output space
	// Number of modes
    plhs[0] = mxCreateDoubleMatrix(1,1, mxREAL);
    kp = mxGetPr(plhs[0]);
	*kp = (double) k;

	// Log-weights
	plhs[1] = mxCreateDoubleMatrix(k,1, mxREAL);
    lnw_o = mxGetPr(plhs[1]);
	memcpy(lnw_o, w_arr, k*sizeof(double));	

	// Means
	plhs[2] = mxCreateDoubleMatrix(nx, k, mxREAL);
    mu_o = mxGetPr(plhs[2]);
	memcpy(mu_o, mu_arr, k*nx*sizeof(double));	
	
	// Covariances
	mwSize dims2[3] = {(mwSize)nx,(mwSize) nx,(mwSize) k};
    plhs[3] = mxCreateNumericArray(3,dims2,mxDOUBLE_CLASS,mxREAL);
	P_o = mxGetPr(plhs[3]);
    memcpy(P_o, P_arr, k*nx2*sizeof(double));
	
// Free malloced space
free(w_arr);
free(mu_arr);
free(P_arr);

}