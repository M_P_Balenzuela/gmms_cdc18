function [M,mu,P,w,Toff] = GMMF_matlab(M,mu,P,w,u,ym,PM,MM,plant_params,KLReduction)

% Unpacking
nx = plant_params.Dimms.nx;
ny = plant_params.Dimms.ny;
nu = plant_params.Dimms.nu;

% Prediction
            % Allocate space for prediction density
            Mp = PM.N*M;
            mup = nan(nx, Mp);
            Pp = nan(nx, nx, Mp);
            wp = nan(Mp,1);
            
            % Perform prediction
            
            for k = 1:PM.N
                % Get SS for PM mode
                A = PM.model(k).A;
                B = PM.model(k).B;
                Q = PM.model(k).Q;
                wm = PM.model(k).wm;
                % Get index range for this mode
                [sidx,eidx] = ith_block(k,M);%PM.N);
                % Mean
                mup(:,sidx:eidx) =  A*mu + B*u;
                % Covariance
                for l=1:M
                    Pp(:,:,sidx + l-1) = A*P(:,:,l)*A' + Q;
                end
                % Weights
                wp(sidx:eidx,1) = w + log(wm);                
            end
            
        
                
            [~, ~, mufone, Pfone] = KL_GMM_reduction_logw(1,1, 1, Mp, wp, mup, Pp);
            Toff = gm2t_logw(1, 0, mufone, Pfone, nx);

        % Correct using measurements
            % Allocate space for filtered density
            Mf = MM.N*Mp;
            muf = nan(nx, Mf);
            Pf = nan(nx, nx, Mf);
            wf = nan(Mf,1);
            % Reset Filtered mode counter
            m=1;
            for k = 1:MM.N
                % load measurement model parameters
                C = MM.model(k).C;
                D = MM.model(k).D;
                R = MM.model(k).R;                
                wm = MM.model(k).wm;
                % Predict measurement using MM
                muy = C*mup + D*u;
                % Apply corrections from this mode of MM
                ML = nan(Mp,1);
                for l = 1:Mp
                    Py = C*Pp(:,:,l)*C' + R;          
                    Kg = Pp(:,:,l)*C'/Py;
                    es = ym - muy(:,l);
                    muf(:,m) = mup(:,l) + Kg*es; 
                    Pf(:,:,m) = Pp(:,:,l) - Kg*Py*Kg';
                    ML(l) = log(det(2*pi*Py)^-0.5) + (-0.5*es'/Py*es);
                    m = m+1;
                end
                [sidx,eidx] = ith_block(k,Mp);
                wf(sidx:eidx,1) = wp + log(wm) + ML; 
            end
            
            
            % Normalise the filtered distribution
            %wf = wf/sum(wf);
            wf = normalise_log_weights(wf);
            
          
             
        
                
        % KL Reduction
        [M, w, mu, P] = KL_GMM_reduction_logw(KLReduction.Ml, KLReduction.Mu, KLReduction.lambda, Mf, wf, muf, Pf);
        
        