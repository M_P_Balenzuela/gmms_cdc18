
%% Clear workspace
clear all;
% close all;
clc;


%% PF/PS params
M_PF_NUMBER = 10^5;%^4%4*10^3%10^4%^3;%10^5;%*10^3; % must be even for the prior chosen



rng('shuffle');
rngseed = round(rand*10^4)
%rngseed=6603
% rngseed = 8147
% rngseed = 925861
%  rngseed = 6603;

% rngseed=5488
% rngseed=6254 % did well
% rngseed=5853
% rngseed = 2156
%  rngseed = 1658
% % rngseed=820;7307;
% rngseed=7307   3523
rngseed = 8190
rng(rngseed);






main_common;
    


    %% Plot results
      

    figure(1);
    clf;
    kidx=[2 10 28];
    
    
a=0.05;
[ha,~] = tight_subplot(1,length(kidx),[a a/2]*2,[a*2 a],[a a]);

for kx =1:length(kidx)+1
    if kx == length(kidx)+1
        figure(2);
        clf;
        k=1;
    else
        
        axes(ha(kx));
        k=kidx(kx);
    
    end
    spaceres = 0.005;%3;
    spaceN = 200;
% Plot GMMS result
tic;
        Ms = GMMS.den(k).M;
        mus = GMMS.den(k).mu;
        Ps = GMMS.den(k).P;
        ws = GMMS.den(k).w;
   
        
        rangestor = nan(2,Ms);
        for i=1:Ms
           rangestor(1,i) = mus(:,i) + 5*Ps(:,:,i)^0.5 ;
           rangestor(2,i) = mus(:,i) - 5*Ps(:,:,i)^0.5 ;
        end

%         xf = PF.den(k).mu;

%         rangestor(1,Ms+1) = max(xf);
%         rangestor(2,Ms+1) = min(xf);


        minS = min(rangestor(2,:));
        maxS = max(rangestor(1,:));
        space = linspace(minS,maxS,spaceN);
        GMspace = space;
%%%
        rangestor = nan(2,Ms+1);
        for i=1:Ms
           rangestor(1,i) = mus(:,i) + 6*Ps(:,:,i)^0.5 ;
           rangestor(2,i) = mus(:,i) - 6*Ps(:,:,i)^0.5 ;
        end

        xf = PF.den(k).mu;

        rangestor(1,Ms+1) = max(xf);
        rangestor(2,Ms+1) = min(xf);


        PSspace = min(rangestor(2,:)):spaceres:max(rangestor(1,:));

%%%

        GMMSr = GM_evaluate_spacelog(ws,mus,Ps,space);
%         subplot(length(kidx)^0.5,length(kidx)^0.5,kx);
%         plot_5(space,GMMSr,1);

tGMMSplot = toc
        




% Now plot aSLDS result
tic;

  
        mus = aSLDS.den(k).mu;
        Ps = aSLDS.den(k).P;
        ws = aSLDS.den(k).w;

        GMMSr = GM_evaluate_spacelog(ws,mus,Ps,space);


%         plot_5(space,GMMSr,2);
tsSLDSplot = toc


% Now plot GPB2S result
tic;

     
        mus = GPB2s.den(k).mu;
        Ps = GPB2s.den(k).P;
        ws = GPB2s.den(k).w;


        GMMSr = GM_evaluate_spacelog(ws,mus,Ps,space);


%         plot_5(space,GMMSr,3);
tGPB2Splot = toc


% Now plot IMMS result
tic;

%         Ms = nz;
        mus = IMMS.den(k).mu;
        Ps = IMMS.den(k).P;
        ws = IMMS.den(k).w;
  
        GMMSr = GM_evaluate_spacelog(ws,mus,Ps,space);


%         plot_5(space,GMMSr,4);
tGPB2Splot = toc
        

% Now plot PS result

        space = PSspace;
        Sd = length(space);
        %
        tic;
        M=M_PF_NUMBER;
        
        % Now plot PS result
        PSr = zeros(1,Sd);

        ym = yidx(:,k);
        u = uidx(:,k);
       

        if (k==1)
           xf = PF_PRIOR;
           wf = -log(M)*ones(M,1);
        else
        wf = PF.den(k-1).w;
        
        xf = PF.den(k-1).mu;

        end 
        
        uf = uidx(:,k+1);
        
        wsf = PS.den(k+1).w;
        xsf = PS.den(k+1).mu;
        
        wT = plot_PS(ym,u,uf,space,MM,PM,xf,wf,xsf,wsf,plant_params);

        plot_5(space,wT,5);

tPSplot = toc

% eval = adrian_plot(PM, M,xsf,wsf,space,u);
% plot(space,eval,'-r');


xlabel('State')
if (kx == length(kidx)+1 || kx ==1)
ylabel('Probability Density')
end
    title(['Smoothed density at k=' num2str(k)])
    disp('subplot finished')



    xlim([minS maxS]);

    if kx ==1 || kx == length(kidx)+1
         legend('PS (Ground Truth)');
    end
end



%%


 disp('FINISHED MAKING PRETTY PICTURES');
 
 figure(1);
 saveas(gcf,'figure1','fig');
 figure(2);
 saveas(gcf,'figure2','fig');
 
%%
send_email_to_mark('Computation finished','CDC run competed, result on DB.');
 
 