function JMLS = GMM_to_JMLS(PM,MM)

z = 0;
nz = PM.N*MM.N;
JMLS.N = nz;
Tp = zeros(nz,1);
for i = 1:PM.N
    for j = 1:MM.N
        z = z+1;
        JMLS.model(z).A = PM.model(i).A;
        JMLS.model(z).B = PM.model(i).B;
        JMLS.model(z).Q = PM.model(i).Q;
        
        JMLS.model(z).C = MM.model(j).C;
        JMLS.model(z).D = MM.model(j).D;
        JMLS.model(z).R = MM.model(j).R;
        
        Tp(z) = log(PM.model(i).wm) + log(MM.model(j).wm);
        
    end
end

JMLS.T = repmat(Tp,1,nz);