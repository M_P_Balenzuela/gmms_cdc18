function [mus,Ps,lnws, musu,Psu, r,s,lnw0] = IMM_smoother(JMLS,r,s,lnwbif,muf,Pf,lnwf,ympr,u,upr, prior)
% Perform one iteration of the IMM smoother, see helmick1995fixed, Fixed-interval smoothing for Markovian switching systems, Helmick, Ronald E and Blair, W Dale and Hoffman, Scott A, 1995

% Convention:  x_{k+1} = Ax_k + Bu_{k+1} y_{k+1} = Cx_{k+1} +Du_{k+1}

% IN:
    % JMLS - JMLS structure
    % r(:,:,j) - Backwards Information Filter Information Matrix for the
    % j-th discrete state
    % s(:,j) - Backwards Information Filter Information Vector for the j-th
    % discrete state
    % lnwbif(j) - Backwards information Filter log-weight for the j-th discrete state 
    % muf(:,i) - Filtered mean for the i-th discrete state
    % Pf(:,:,i) - Filtered covariance for the i-th discrete state
    % lnwf(i) - Filtered log-weight for the i-th discrete state
    % ympr - Measurement vector at
    % u - The input vector which would be used to propagate the Filtered
    % distribution into the next timestep
    % upr - The input vector which was last used to correct the filtered
    % distribution
    
% OUT:
    % mus(:,i) - Mean for the i-th discrete state of the smoothed distribution
    % Ps(:,:,i) - Covariance for the i-th discrete state of the smoothed distibution
    % lnws(i) - Log-weight for the i-th discrete state of the smoothed distibution
    % musu - Mean for the unimodal approximation fo the smoothed
    % distribution
    % Psu - Covariance for the unimodal approximation of the smoothed
    % distribution
    % r(:,:,i) - Information Matrix for the i-th discrete state in the updated BIF likelihood
    % s(:,i) - Information Vector for the i-th discrete state in the updated BIF likelihood
    % lnw0(i) - Log-weight for the i-th discrete state in the updated BIF likelihood

    
    
    % Note that JMLS.T(i,j) = pji;
    
nz = JMLS.N;
nx = size(muf,1);

% Allocate space
Pbif = nan(nx,nx,nz);
mubif = nan(nx,nz);
for j = 1:nz
    % Backwards propagate the j-th mode with j-th model
    A = JMLS.model(j).A;
    B = JMLS.model(j).B;
    Q = JMLS.model(j).Q;
    invQ = Q^(-1);
    
    rl = r(:,:,j);
    sl = s(:,j);

    O = ((rl +invQ)')^(-1);
    ril = A'*invQ*(eye(nx) - O*invQ)*A;
    Bu = B*u;
    sil = A'*invQ*(Bu+O*sl-O*invQ*Bu);
    
    [mubifj,Pbifj] = IF_to_normal(ril,sil);
    Pbif(:,:,j) = Pbifj;
    mubif(:,j) = mubifj;
end

lnw0 = nan(nz,1);
P0 = nan(nx,nx,nz);
mu0 = nan(nx,nz);
wij = JMLS.T + repmat(prior.',nz,1); % (58)
wij = normalise_log_weights(wij.').'; % (58) + (59)
wij = wij + repmat(lnwbif,1,nz); % (55)
for i=1:nz
    lnw0(i) = LSE(wij(:,i)); % aj (56)
    wn = normalise_log_weights(wij(:,i)); % (55) + (56)
    [~, ~, musu, Psu] = GM_reduction_KL(wn, mubif, Pbif,1,1);
    mu0(:,i) = musu;
    P0(:,:,i) = Psu; 
end
lnw0 = normalise_log_weights(lnw0);
% w0b = w0;
for j=1:nz
    C = JMLS.model(j).C;
    D = JMLS.model(j).D;
    R = JMLS.model(j).R;
    invR = R^(-1);
    
    mu0j = mu0(:,j);
    P0j = P0(:,:,j);
    
    Py = C*P0j*C' + R; % (52)
    lnw0(j) = lnw0(j) + N_loglikelihood(ympr,C*mu0j + D*upr,Py); % (43) lnw0 + Lambdabjk   %Lambdabjk is made with (51) + (67)
    
    [ril,sil] = normal_to_IF(mu0j,P0j);
    r(:,:,j) = ril + C'*invR*C;
    s(:,j) = sil + C'*invR*(D*upr - ympr);
    
end

lnw0 = normalise_log_weights(lnw0); % (43) + (45)

% Allocate space
Ps = nan(nx,nx,nz);
mus = nan(nx,nz);
% ws = nan(nz,1);

% Combine with filtering dist.
muij = nan(nx,nz);
Pij = nan(nx,nx,nz);
wij = JMLS.T; % + wbif
for j = 1:nz %filtered index
    mufj = muf(:,j);
    Pfj = Pf(:,:,j);
    for i=1:nz % BIF index
        mu0i = mubif(:,i);%mu0
        P0i = Pbif(:,:,i);%P0
        
        % combine over BIF
        [mu,P] = normal_combine_two_modes(mufj,Pfj,mu0i,P0i); % (82) + (83)
        muij(:,i) = mu;
        Pij(:,:,i) = P;
        wij(i,j) = wij(i,j) + N_loglikelihood(mu0i,mufj,P0i + Pfj); %wij + Lambdakji (74)% Lambdakji is made with (88) + (89) + (90) 
    end
    wn = normalise_log_weights(wij(:,j)); % (74) + (76)
    [~, ~, musu, Psu] = GM_reduction_KL(wn, muij, Pij,1,1); % (77) + (78)
    mus(:,j) = musu;
    Ps(:,:,j) = Psu; 
    %ws(j) = w0b(j) + wf(j);
end


wb = LSE(wij)'; % dj (76)
% wb = normalise_log_weights(wb); 
lnws = wb + lnwf; %(69)
lnws = normalise_log_weights(lnws); %(69) + % (70)
[~, ~, musu, Psu] = GM_reduction_KL(lnws, mus, Ps,1,1); % (71) + (72)

