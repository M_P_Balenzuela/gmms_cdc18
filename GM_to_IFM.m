function [r,s,t] = GM_to_IFM(widx, muidx, Pidx)
% Convert a Gaussian mixture distribution into Information form mixture

% IN:
    % widx(i) - Log-weight of i-th component
    % muidx(:,i) - Mean of i-th component 
    % Pidx(:,:,i) - Covariance of i-th component
    
% OUT:
    % r(:,:,i) - Information matrix for i-th component
    % s(:,i) - Information vector of i-th component
    % t(i) - Information weight of i-th component

[nx,M] = size(muidx);

r = nan(nx,nx,M);
s = nan(nx,M);
t = nan(M,1);


for k = 1:M
    mu = muidx(:,k);
    P = Pidx(:,:,k);
    w = widx(k);
    
    r(:,:,k) = eye(nx)/P;
    s(:,k) = -eye(nx)/P*mu;
    t(k) = mu'/P*mu - 2*(log(det(2*pi*P)^-0.5) + w);
end

end