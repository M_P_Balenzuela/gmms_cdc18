function [M, r, s, t] = IFM_combiner(ra, sa, ta, rb, sb, tb)
% Find the combinations of two distributions defined by Information form
% mixtures

% IN:
    % ra(:,:,i) - Information matrix for the i-th component in mixture A
    % sa(:,i) - Information vector for the i-th component in mixture A
    % ta(i) - Information weight for the i-th component in mixture A
    % rb(:,:,j) - Information matrix for the j-th component in mixture B
    % sb(:,j) - Information vector for the j-th component in mixture B
    % tb(j) - Information weight for the j-th component in mixture B
    
% OUT:
    % M - Number of components in the output mixture
    % r(:,:,i) - Information matrix for the i-th component in the output mixture
    % r(:,i) - Information vector for the i-th component in the output mixture
    % t(i) - Information weight for the i-th component in the output mixture
    
[nx,Ma] = size(sa);
[nxb,Mb] = size(sb);

assert(nx == nxb, 'State vector must have a consistent length!');

M = Ma*Mb;
r = nan(nx,nx,M);
s = nan(nx,M);
t = nan(M,1);

idx = 0;
for i = 1:Ma
    rai = ra(:,:,i);
    sai = sa(:,i);
    tai = ta(i);
    for j = 1:Mb
            rbj = rb(:,:,j);
            sbj = sb(:,j);
            tbj = tb(j);
 
            idx = idx + 1;
            
            r(:,:,idx) = rai + rbj;
            s(:,idx) = sai + sbj;
            t(idx) = tai + tbj;
            
    end
end
